# SockLib

SockLib is a C++ library which allows to use sockets under Linux.



## Compilation

To compile SockLib under Linux, you need:
- Git
- Make

First, clone the **SockLib** repository:

```
git clone https://gitlab.com/Crumble14/SockLib
```

Then, go inside the SockLib folder and build the project:

```
cd SockLib
make
```

The created **.a** file can be used in any C++ project as a static library.



## TCP

The ``Socket`` and ``ServerSocket`` classes of the ``socket.hpp`` header allow to communicate with other devices through network using the TCP protocol.



### HTTP

The ``HTTPCall`` class of the ``socket.hpp`` header allows to perform HTTP calls.



## UDP

The ``DatagramSocket`` class of the ``socket.hpp`` header allows to communicate with other devices through network using the UDP protocol.
