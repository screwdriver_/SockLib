NAME = sockLib
COMPILER = gcc
FLAGS = -Wall -Wextra -Werror -std=c++17 -pthread

SRC = src/*.cpp

all: $(NAME)

$(NAME):
	@$(COMPILER) $(FLAGS) -c $(SRC)
	@ar rc $(NAME).a *.o

clean:
	@rm -f *.o

fclean: clean
	@rm -f $(NAME).a

re: fclean all
